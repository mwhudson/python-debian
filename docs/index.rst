Documentation for the `debian` module
======================================

.. include:: ../README.rst


Contents

.. toctree::
   :maxdepth: 1

   contributing
   credits


API Documentation:

.. toctree::
   :maxdepth: 1

   api/debian
   api/debian.arfile
   api/debian.changelog
   api/debian.copyright
   api/debian.deb822
   api/debian.debfile
   api/debian.debian_support
   api/debian.debtags
   api/debian.deprecation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
